import React from 'react';
import Calculator from './Calculator';
import {
  BrowserRouter as Router,
  Route
} from "react-router-dom";

class App extends React.Component {
  
  render() {
    return (
      <Router>
        <Route>
          <Calculator dataset={this.props.dataset} />
        </Route>
      </Router>
    );
  }
}


export default App;
