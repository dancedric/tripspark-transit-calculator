import React from 'react';
import NumberFormat from 'react-number-format';
import './App.css';


class Calculator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tripsPerDay: 0,
      noShowRate: 0,
      costPerNoShow: 0,
      percentReductionInNoShows: 0,
      days: 0,
      savings: 0
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handlePercentChange = this.handlePercentChange.bind(this);
    this.calculateSavings = this.calculateSavings.bind(this);
  }

  componentDidMount() {
    const dataset = this.props.dataset;
    console.log(dataset);
    this.setState({
      tripsPerDay: 250,
      noShowRate: 5,
      costPerNoShow: 30,
      percentReductionInNoShows: 30,
      days: 252,
      savings: 0
    });

    if('tripsPerDay' in dataset) this.setState({tripsPerDay: dataset.tripsPerDay});
    if('noShowRate' in dataset) this.setState({noShowRate: dataset.noShowRate});
    if('costPerNoShow' in dataset) this.setState({costPerNoShow: dataset.costPerNoShow});
    if('percentReductionInNoShows' in dataset) this.setState({percentReductionInNoShows: dataset.percentReductionInNoShows});

    this.calculateSavings();
  }

  handleInputChange(e) {
    this.setState({
      [e.target.name] : Number(e.target.value)
    }, this.calculateSavings );
  }

  handlePercentChange(e) {
    console.log(e.target);
  }

  calculateSavings() {
    let result = this.state.tripsPerDay * (this.state.noShowRate/100) * this.state.costPerNoShow * (this.state.percentReductionInNoShows/100) * this.state.days;
    this.setState({savings: result});
  }


  render() {
    return (
      <div className="App dark">
        <div className="design-piece">
          <img src="https://tripspark.com/assets/js/transit_calculator/Icon-lines.svg" alt="TripSpark Transit Calculator divider" />
        </div>
        <div className="form-row">
          <p className="label">Trips Per Day</p>
          <input type="text" format="number" className="text" name="tripsPerDay" value={this.state.tripsPerDay} onChange={this.handleInputChange} />
        </div>
        <div className="form-row">
          <p className="label">No-Show Rate</p>
          <NumberFormat value={this.state.noShowRate} displayType={'input'} isNumericString={true} className="text" name="noShowRate" suffix={'%'} onValueChange={(values) => { this.setState({noShowRate: values.value}, function() {this.calculateSavings(); })} } />
      </div>
        <div className="form-row">
          <p className="label">Cost per No-Show</p>
          <NumberFormat value={this.state.costPerNoShow} displayType={'input'} isNumericString={true} className="text" name="costPerNoShow" thousandSeparator={true} prefix={'$'} onValueChange={(values) => { this.setState({costPerNoShow: values.value}, function() {this.calculateSavings(); })} } />
      </div>
        <div className="form-row">
          <p className="label">% Reduction in No-Shows</p>
          <NumberFormat value={this.state.percentReductionInNoShows} displayType={'input'} isNumericString={true} className="text" name="percentReductionInNoShows" suffix={'%'} onValueChange={(values) => { this.setState({percentReductionInNoShows: values.value}, function() {this.calculateSavings(); })} } />
      </div>
        <div className="form-row highlighted">
          <p className="label">Yearly Savings</p>
          <NumberFormat value={this.state.savings} displayType={'input'} isNumericString={true} className="text" name="savings" thousandSeparator={true} prefix={'$'} readOnly decimalScale={0} />
        </div>
      </div>
    );
  }
}

export default Calculator;
